'use strict';

const languages = [
  {code: "ar", name: "العربية", direction: "rtl"},
  {code: "de", name: "Deutsch", direction: "ltr"},
  {code: "en", name: "English", direction: "ltr"},
  {code: "es", name: "Español", direction: "ltr"},
  {code: "fr", name: "Français", direction: "ltr"},
  {code: "hi", name: "हिन्दी", direction: "ltr"},
  {code: "it", name: "Italiano", direction: "ltr"},
  {code: "ja", name: "日本語", direction: "ltr"},
  {code: "ko", name: "한국어", direction: "ltr"},
  {code: "zh", name: "中文", direction: "ltr"},
]
const defaultLanguage = "en"

window.onload = function() {
  const languageInput = document.getElementById('language')

  for (var i = 0; i < languages.length; i++) {
    const object = document.createElement("option")
    object.setAttribute("value", languages[i].code)
    object.setAttribute("dir", languages[i].direction)
    object.innerHTML = languages[i].name

    if (languages[i].code == defaultLanguage ) {
      object.setAttribute("selected", true)
    }

    languageInput.appendChild(object)
  }
}

function showTableOfContents() {
  const title = document.getElementById('article-title').value
  const languageInput = document.getElementById('language')
  const language = languageInput.value
  const dir = languageInput.options[languageInput.selectedIndex].getAttribute("dir")
  const pageURL = generatePageURL(title, language)

  fetchDetails(title, language, function() {
    document.documentElement.setAttribute("dir", dir)
    const content = document.getElementById('content')
    content.innerHTML = '';

    if (this.status == 200) {
      const result = JSON.parse(this.response)
      for (var i = 0; i < result.toc.entries.length; i++) {
        const item = createElement(result.toc.entries[i], pageURL, dir)
        content.appendChild(item)
      }
    } else if (this.status == 404) {
      content.innerHTML = 'This article doesn\'t exist in the selected language'
    } else {
      content.innerHTML = 'Status ' + this.status + ': ' + this.statusText
    }
  })
}

function fetchDetails(title, language, onload) {
  const apiURL = generateAPIURL(title, language)
  const xhr = new XMLHttpRequest()
  xhr.open("GET", apiURL)
  xhr.send()
  xhr.onerror = function() {
    alert("Error fetching the webpage")
  };
  xhr.onload = onload;
}

function createElement(item, pageURL, dir) {
  const container = document.createElement("li")
  const link = document.createElement("a")
  container.appendChild(link)
  container.setAttribute("dir", dir)

  const sectionNumber = document.createElement("span")
  sectionNumber.innerHTML = item.number

  link.appendChild(sectionNumber)
  link.innerHTML += item.html
  link.style.paddingLeft = item.level + 'em';
  link.setAttribute("href", pageURL + "#" + item.anchor)
  link.setAttribute("target", "_blank")

  return container
}

function generateAPIURL(title, language) {
  title = encodeURI(title)
  return "https://" + language + ".wikipedia.org/api/rest_v1/page/metadata/" + title + "?redirect=true"
}

function generatePageURL(title, language) {
  title = encodeURI(title)
  return "https://" + language + ".wikipedia.org/wiki/" + title
}